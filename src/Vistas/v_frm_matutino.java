/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Alto
 */
public class v_frm_matutino extends JFrame{
          private JPanel panelNorte , panelCentro, panelEste, panelSur;
    public JButton bo1,bo2,bo3,bo4,bo5,bo6;
    public JTextField tf1,tf2,tf3,tf4;
    public JLabel lbl1,lbl2,lbl3,lbl4;
    
    public v_frm_matutino(){
    marco();
    }
    
   public void marco(){
      
     // JFrame f= new JFrame();
      this.setSize(500,350);
      this.setTitle("Horario Matutino");
      this.setLocation(200,200);
      this.setVisible(true);
      this.setResizable(false);

  
    JPanel panelMayor= new JPanel(new BorderLayout());
    
     panelNorte= new JPanel();
     panelCentro= new JPanel();
     panelEste= new JPanel();
     panelSur= new JPanel();

            FlowLayout fl1= new FlowLayout(FlowLayout.CENTER);
            panelNorte.setLayout(fl1);
            
            GridBagLayout gbl1= new GridBagLayout();
            GridBagConstraints gbc1= new GridBagConstraints();
            panelCentro.setLayout(gbl1);
            
            GridBagLayout gbl2= new GridBagLayout();
            GridBagConstraints gbc2= new GridBagConstraints();
            panelEste.setLayout(gbl2);
            
            GridBagLayout gbl3= new GridBagLayout();
            GridBagConstraints gbc3= new GridBagConstraints();
            panelSur.setLayout(gbl3);


            JLabel lblTitulo=new JLabel("Candy Overchange");
        
             //COMPONNETES PARA PANEL CENTRO
        lbl1 = new JLabel("Clave: ");
        tf1 = new JTextField(25);
        lbl2 = new JLabel("Nombre: ");
        tf2 = new JTextField(25);
        lbl3 = new JLabel("Direccion: ");
        tf3 = new JTextField(25);
        lbl4= new JLabel("Telefono: ");
        tf4= new JTextField(25);
        
        lbl1.setFont( new Font( "Helvetica", Font.BOLD, 12 ) );
        lbl2.setFont( new Font( "Helvetica", Font.BOLD, 12 ) );
        lbl3.setFont( new Font( "Helvetica", Font.BOLD, 12 ) );
        lbl4.setFont( new Font( "Helvetica", Font.BOLD, 12 ) );
        
        
        
       
        //Componentes para panel este
             bo1= new JButton();
             bo2= new JButton();
             bo3= new JButton();
             bo4= new JButton();
             bo5= new JButton();
             bo6= new JButton();
           
            ImageIcon ic1 = new ImageIcon("icon/1.png");
            bo1.setIcon(ic1);
            ImageIcon ic2 = new ImageIcon("icon/2.png");
            bo2.setIcon(ic2);
            ImageIcon ic3 = new ImageIcon("icon/3.png");
            bo3.setIcon(ic3);
            ImageIcon ic4 = new ImageIcon("icon/4.png");
            bo4.setIcon(ic4);
            ImageIcon ic5 = new ImageIcon("icon/5.png");
            bo5.setIcon(ic5);
            ImageIcon ic6 = new ImageIcon("icon/6.png");
            bo6.setIcon(ic6);
           
            bo1.setToolTipText("Agregar");
            bo2.setToolTipText("Cancelar");
            bo3.setToolTipText("Editar");
            bo4.setToolTipText("Guardar");
            bo5.setToolTipText("Eliminar");
            bo6.setToolTipText("Salir");
            
            bo1.setPreferredSize(new Dimension(50, 50));
            bo2.setPreferredSize(new Dimension(50, 50));
            bo3.setPreferredSize(new Dimension(50, 50));
            bo4.setPreferredSize(new Dimension(50, 50));
            bo5.setPreferredSize(new Dimension(50, 50));
            bo6.setPreferredSize(new Dimension(50, 50));
            
         //Agregadno componentes a paneles
        lblTitulo.setFont( new Font( "Helvetica", Font.BOLD, 18 ) );
         panelNorte.add(lblTitulo);
         
         gbc1.anchor= GridBagConstraints.WEST;        
         //gbc1.gridwidth=0;
         
         
         //Centro
         gbc1.gridx=0;
         gbc1.gridy=0;
         panelCentro.add(lbl1,gbc1);
         gbc1.gridy= 0;
         gbc1.gridx=1;
         panelCentro.add(tf1,gbc1);
         
         gbc1.gridy=1;
         gbc1.gridx=0;
         panelCentro.add(lbl2,gbc1);
         gbc1.gridy=1;
         gbc1.gridx=1;
         panelCentro.add(tf2,gbc1);
         
         gbc1.gridx=0;
         gbc1.gridy= 2;
         panelCentro.add(lbl3,gbc1);
         gbc1.gridx=1;
         gbc1.gridy= 2;
         panelCentro.add(tf3,gbc1);
         
         gbc1.gridx=0;
         gbc1.gridy= 3;
         panelCentro.add(lbl4,gbc1);
         gbc1.gridx=1;
         gbc1.gridy= 3;
         panelCentro.add(tf4,gbc1);

         
         //Este
         gbc2.anchor= GridBagConstraints.EAST; 
         panelEste.add(bo1,gbc2);
         gbc2.gridy= 1;
         gbc2.gridwidth= 5;
         panelEste.add(bo2,gbc2);
         gbc2.gridwidth= 1;
         gbc2.gridy=2;
         panelEste.add(bo3,gbc2);
         gbc2.gridy=3;
         gbc2.gridwidth= 5;
         panelEste.add(bo4,gbc2);
         gbc2.gridwidth= 1;
         gbc2.gridy= 4;
         panelEste.add(bo5,gbc2); 
         gbc2.gridwidth= 5;
         gbc2.gridy= 5;
         panelEste.add(bo6,gbc2);
         
        
         
        panelMayor.add(panelNorte,BorderLayout.NORTH);
        panelMayor.add(panelCentro,BorderLayout.CENTER);
        panelMayor.add(panelEste,BorderLayout.EAST);
        panelMayor.add(panelSur,BorderLayout.WEST);

           //Asociar el panel mayor a la forma o ventaa
           this.add(panelMayor);
           
        
        
  //  f.setVisible(true);
    
  
  
  }
}
